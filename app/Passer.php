<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Passer extends Model
{
  protected $table = 'passers';
  protected $fillable = ['name', 'eligibility', 'school', 'division'];
}
