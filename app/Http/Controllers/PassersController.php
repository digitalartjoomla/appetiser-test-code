<?php

namespace App\Http\Controllers;
use DB;
use App\Passer;
use App\Http\Resources\PasserResource;
use Illuminate\Http\Request;

class PassersController extends Controller
{
  public function index(Request $request)
  {
    $search = $request->get('search');

    $passers = Passer::where(function($query) use ($search) {
      if($search && $search != 'null'){
        $query
          ->where('name', 'like', '%'.$search.'%')
          ->orWhere('eligibility', 'like', '%'.$search.'%')
          ->orWhere('school', 'like', '%'.$search.'%')
          ->orWhere('division', 'like', '%'.$search.'%');
      }
    })->orderBy('name', 'asc')->paginate(50);

    return PasserResource::collection($passers)->additional(['meta' => [
        'search' => 'search='.$search
    ]]);
  }

  public function store(Request $request)
  {
    $data = $request->all();

    $passer_data = [
      'name' => $data['name'],
      'eligibility' => $data['eligibility'],
      'school' => $data['school'],
      'division' => $data['division'],
    ];

    if(Passer::create($passer_data)){
      return 200;
    }
  }

  public function getTotals()
  {

    $totals = Passer::select('school', DB::raw('count(school) quantity'))->groupBy('school')->orderBy('quantity', 'desc')->orderBy('school', 'asc')->get();

    return $totals;
  }
}
